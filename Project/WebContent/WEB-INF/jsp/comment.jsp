
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>comment</title>
</head>
<body>
	<div class="row navbar-dark bg-dark">
		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<li class="navbar-text">${userInfo.userName}さん</li>

		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<div class="col-sm-0">
			<a href="logoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>
	</div>
<div class="row mt-5">
	<div class="col-sm-4"></div>
		<h1>コメント一覧</h1>
		</div>
	<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">タイトル:</div>
			<div class="col-sm-0">${board.boardName}</div>
		</div>


		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">投稿者:</div>
			<div class="col-sm-0">${board.userName}</div>
		</div>


		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">投稿日時:</div>
			<div class="col-sm-0">${board.createDate}</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">本文:</div>
			<div class="col-sm-0">${board.content}</div>
		</div>


		<link rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
			integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
			crossorigin="anonymous">

	<form action="CommentServlet" method="post">
		<table class="table table-bordered mt-5">
			<thead>
				<tr class="table-active">
					<th scope="col">投稿者</th>
					<th scope="col">コメント</th>
					<th scope="col"></th>
				</tr>

			</thead>
			<tbody>
				<c:forEach var="come" items="${comment}">
					<tr>
						<td>${come.username}</td>
						<td>${come.boardname}</td>

						<td><c:if test="${userInfo.userName != come.username}">
								<c:if test="${userInfo.loginId == 'login'}">
									<a class="btn btn-primary"
										href="CommentDeleteServlet?id=${come.id}">削除</a>
								</c:if>
							</c:if> <c:if test="${userInfo.userName == come.username}">
								<a class="btn btn-primary"
									href="CommentDeleteServlet?id=${come.id}">削除</a>

							</c:if>
					</tr>
				</c:forEach>
			</tbody>
		</table>


		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-4"></div>
			<div class="col-sm-3">

				<textarea name="freeans" rows="10" cols="40">コメントを追加してください</textarea>
			</div>

		</div>

		<input type="hidden" name="userName" value="${userInfo.userName}"
			readonly style="width: 200px;"> <input type="hidden"
			name="boardName" value="${board.boardName}" readonly
			style="width: 200px;">



		<div class="row mt-5">
			<div class="col-sm-9"></div>
			<div class="col-sm-2">
				<input type="submit" value="送信">
			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-1"></div>

			<INPUT type="button" class="btn btn-link" onclick="history.back()"
				value="戻る">
		</div>
	</form>
</body>
</html>