<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>


<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

</style>

<body>


	<form action="registerServlet" method="post">

		<div class="row navbar-dark bg-dark">
			<div class="col-sm-5"></div>
			<div class="col-sm-6 navbar-brand"></div>
			<div class="col-sm-">
				<a href="logoutServlet" class="navbar-link logout-link">ログアウト</a>
			</div>
		</div>


		<div class="row mt-5">
			<div class="col-sm-5"></div>

			<div class="col-sm-7">
				<h1>ユーザー新規登録</h1>
			</div>
		</div>

		<div class="col-sm-5"></div>
		<div class="col-sm-7">
			<c:if test="${errMsg != null}">
				<span class="badge badge-pill badge-danger">${errMsg}</span>
			</c:if>
		</div>

		<div class="row mt-5">
			<div class="col-sm-5"></div>
			<div class="col-sm-1">ログインID</div>
			<div class="col-sm-4">
				<input type="text" name="loginId" style="width: 200px;">

			</div>
		</div>


		<div class="row mt-5">
			<div class="col-sm-5"></div>
			<div class="col-sm-1">パスワード</div>
			<div class="col-sm-1">
				<input type="text" name="password" style="width: 200px;">

			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-5"></div>
			<div class="col-sm-1">パスワード(確認)</div>
			<div class="col-sm-4">
				<input type="text" name="checkPassword" style="width: 200px;">

			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-5"></div>
			<div class="col-sm-1">ユーザー名</div>
			<div class="col-sm-4">
				<input type="text" name="userName" style="width: 200px;">

			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-5"></div>
			<div class="col-sm-1">生年月日</div>
			<div class="col-sm-4">
				<input type="date" name="birthDate" style="width: 200px;">

			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-6"></div>
			<div class="col-sm-2">
				<input type="submit" value="登録">
			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-1"></div>

			<INPUT type="button" class="btn btn-link" onclick="history.back()"
				value="戻る">
		</div>




	</form>
</body>
</html>