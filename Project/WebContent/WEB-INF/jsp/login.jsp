<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<meta charset="UTF-8">
<title>login</title>
</head>
<body>

	<form action="/MyWebsite/loginServlet" method="post">
		<c:if test="${errMsg != null}">

			<span class="badge badge-pill badge-danger">${errMsg}</span>
		</c:if>
		<span class="badge badge-pill badge-success">${logout}</span>


		<div class="row mt-5">
			<div class="col-sm-5"></div>

			<h1>ログイン</h1>
		</div>
		<div class="row mt-5">
			<div class="col-sm-3"></div>
			<div class="col-sm-2">ログインID</div>
			<div class="col-sm-4">
				<input type="text" name="loginId" style="width: 200px;">
				<div class="col-sm-4"></div>
			</div>
		</div>

		<div class="row mt-3">
			<div class="col-sm-3"></div>
			<div class="col-sm-2">パスワード</div>
			<div class="col-sm-4">


				<input type="text" name="password" style="width: 200px;">
				<div class="col-sm-4"></div>
			</div>
		</div>


		<div class="col-sm-2 mx-auto mt-5">
			<input type="submit" value="ログイン">
		</div>

	</form>
</body>
</html>