<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>コメント削除確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="style.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<form action="CommentDeleteServlet" method="post">
		<input type="hidden" name="loginId" value="${user.loginId}">
		<div class="row navbar-dark bg-dark">
		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<li class="navbar-text">${userInfo.userName}さん</li>

		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<div class="col-sm-0">
			<a href="logoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>
	</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			コメント削除確認
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-0">コメント内容：</div>
			<div class="col-sm-4">
			<input type="hidden" name="commentId" value="${comment.id}">
				${comment.boardname}

			</div>
		</div>

		<div class=row>
			<div class="col-sm-4"></div>
			を本当に削除してよろしいでしょうか。
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-3">
				<button type="button" onclick="history.back()">キャンセル</button>
			</div>
			<input type="submit" value="OK">
		</div>

		<div class="row mt-5">
			<div class="col-sm-1"></div>

			<INPUT type="button" class="btn btn-link" onclick="history.back()"
				value="戻る">
		</div>


	</form>
</body>
</html>