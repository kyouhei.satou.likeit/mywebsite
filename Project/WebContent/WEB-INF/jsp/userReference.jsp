<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー参照</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">


</head>
<body>



	<form action="userUpdateServlet" method="post">
		<div class="row navbar-dark bg-dark">
		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<li class="navbar-text">${userInfo.userName}さん</li>

		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<div class="col-sm-0">
			<a href="logoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>
	</div>

		<div class="row mt-5">

			<div class="col-sm-4"></div>
			<div>ユーザー情報参照</div>
		</div>


		</style>


		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">ログインID</div>
			<div class="col-sm-0">
				<input type="hidden" name="loginId" value="${user.loginId}">
				${user.loginId}
			</div>
		</div>


		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">ユーザー名</div>
			<div class="col-sm-0">${user.userName}</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">
				<j1>生年月日</j1>
			</div>
			<div class="col-sm-0">${user.birthDate}</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">登録日時</div>
			<div class="col-sm-0">${user.createDate}</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">更新日時</div>
			<div class="col-sm-0">${user.updateDate}</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-1">
				<INPUT type="button" class="btn btn-link" onclick="history.back()"
					value="戻る">
			</div>
		</div>



	</form>
</body>
</html>