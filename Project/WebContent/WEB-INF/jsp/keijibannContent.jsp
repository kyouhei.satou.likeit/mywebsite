<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">


</head>
<body>




		<div class="row navbar-dark bg-dark">
			<div class="col-sm-5"></div>
			<div class="col-sm-5 navbar-brand"></div>
			<li class="navbar-text">${userInfo.userName}さん</li>

			<div class="col-sm-5"></div>
			<div class="col-sm-5 navbar-brand"></div>
			<div class="col-sm-0">
				<a href="logoutServlet" class="navbar-link logout-link">ログアウト</a>
			</div>
		</div>

		<div class="row mt-5">

			<div class="col-sm-4"></div>
			<h1>掲示板閲覧</h1>
		</div>


		</style>


		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">タイトル:</div>
			<div class="col-sm-0">${board.boardName}</div>
		</div>


		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">投稿者:</div>
			<div class="col-sm-0">${board.userName}</div>
		</div>


		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">投稿日時:</div>
			<div class="col-sm-0">${board.createDate}</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">本文:</div>
			<div class="col-sm-0">${board.content}</div>
		</div>















			<div class="row mt-5">
				<div class="col-sm-5">
					<INPUT type="button" class="btn btn-link" onclick="history.back()"
						value="戻る">
				</div>
			</div>


</body>
</html>