<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>掲示板閲覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>
<body>

	<div class="row navbar-dark bg-dark">
		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<li class="navbar-text">${userInfo.userName}さん</li>

		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<div class="col-sm-0">
			<a href="logoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>
	</div>

	<div class="row mt-5">
		<div class="col-sm-5"></div>
		<h1>掲示板一覧</h1>
	</div>



	<div class="row mt-5">
		<div class="col-sm-10"></div>

		<a class="btn btn-primary" href="KeijibannCreateServlet" role="button">掲示板記事作成</a>
	</div>
	<div class="row mt-5">
		<div class="col-sm-10"></div>
		<a class="btn btn-primary" href="userListServlet" role="button">ユーザーリスト</a>
	</div>



	<table class="table table-bordered mt-5">
		<thead>

			<tr class="table-active">

				<th scope="col">タイトル</th>
				<th scope="col">投稿者</th>
				<th scope="col">投稿日時</th>
				<th scope="col"></th>

			</tr>


		</thead>


		<tbody>
			<c:forEach var="board" items="${boardList}">
				<tr>

					<td>${board.boardName}</td>
					<td>${board.userName}</td>
					<td>${board.createDate}</td>
					<td><c:if test="${userInfo.userName != board.userName}">
							<a class="btn btn-primary"
								href="KeijibannContentServlet?id=${board.id}">本文</a>
							<a class="btn btn-primary"
								href="CommentServlet?name=${board.boardName}">コメント欄</a>

							<c:if test="${userInfo.loginId == 'login'}">
								<a class="btn btn-primary"
									href="CommentServlet?name=${board.boardName}">コメント欄</a>

								<a class="btn btn-primary"
									href="KeijibannDeleteServlet?id=${board.id}">削除</a>
							</c:if>
						</c:if> <c:if test="${userInfo.userName == board.userName}">
							<a class="btn btn-primary"
								href="KeijibannContentServlet?id=${board.id}">本文</a>
							<a class="btn btn-primary"
								href="CommentServlet?name=${board.boardName}">コメント欄</a>

							<a class="btn btn-primary"
								href="KeijibannDeleteServlet?id=${board.id}">削除</a>



						</c:if>
				</tr>


			</c:forEach>
		</tbody>

	</table>
	<div class="row mt-5">
		<div class="col-sm-1"></div>

		<INPUT type="button" class="btn btn-link" onclick="history.back()"
			value="戻る">
	</div>



</body>
</html>