<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>


<html>
<head>
<meta charset="UTF-8">
<title>掲示板記事作成</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>



<body>


	<form action="KeijibannCreateServlet" method="post">

		<div class="row navbar-dark bg-dark">
		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<li class="navbar-text">${userInfo.userName}さん</li>

		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<div class="col-sm-0">
			<a href="logoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>
	</div>


		<div class="row mt-5">
			<div class="col-sm-5"></div>

			<div class="col-sm-7">
				<h1>記事作成</h1>
			</div>
		</div>

		<div class="col-sm-5"></div>
		<div class="col-sm-7"></div>

		<div class="row mt-5">
			<div class="col-sm-5"></div>
			<div class="col-sm-1">投稿者</div>
			<div class="col-sm-4">
				<input type="text" name="userName" value="${userInfo.userName}" readonly
					style="width: 200px;">

			</div>
		</div>


		<div class="row mt-5">
			<div class="col-sm-5"></div>
			<div class="col-sm-1">タイトル</div>
			<div class="col-sm-1">
				<input type="text" name="title" value="" style="width: 200px;">

			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-5"></div>
			<div class="col-sm-1">本文</div>
			<div class="col-sm-4">

				<textarea name="freeans" rows="10" cols="40">本文を入力してください</textarea>

			</div>
		</div>



		<div class="row mt-5">
			<div class="col-sm-6"></div>
			<div class="col-sm-2">
				<input type="submit" value="投稿">
			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-1"></div>

			<INPUT type="button" class="btn btn-link" onclick="history.back()"
				value="戻る">
		</div>




	</form>
</body>
</html>