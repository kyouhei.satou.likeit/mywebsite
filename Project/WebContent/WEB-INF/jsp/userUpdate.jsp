<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">


</head>
<body>



	<form action="userUpdateServlet" method="post">
		<div class="row navbar-dark bg-dark">
		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<li class="navbar-text">${userInfo.userName}さん</li>

		<div class="col-sm-5"></div>
		<div class="col-sm-5 navbar-brand"></div>
		<div class="col-sm-0">
			<a href="logoutServlet" class="navbar-link logout-link">ログアウト</a>
		</div>
	</div>

		<div class="row mt-5">

			<div class="col-sm-4"></div>
			<div>ユーザー情報更新</div>
		</div>

		<div class="col-sm-5"></div>
		<div class="col-sm-7">
			<c:if test="${errMsg != null}">
				<span class="badge badge-pill badge-danger">${errMsg}</span>
			</c:if>
		</div>


		</style>


		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">
				<j1>ログインID</j1>
			</div>
			<div class="col-sm-0">
				<input type="hidden" name="loginId" value="${user.loginId}">
				${user.loginId}
			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">
				<j1>パスワード</j1>
			</div>
			<div class="col-sm-0">
				<input type="text" name="password" style="width: 200px;">
			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">
				<j1>パスワード(確認)</j1>
			</div>
			<div class="col-sm-0">
				<input type="text" name="checkPassword" style="width: 200px;">
			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">
				<j1>ユーザー名</j1>
			</div>
			<div class="col-sm-0">
				<input type="text" name="userName" value="${user.userName}"
					style="width: 200px;">
			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-4"></div>
			<div class="col-sm-2">
				<j1>生年月日</j1>
			</div>
			<div class="col-sm-0">
				<input type="DATE" name="birthDate" value="${user.birthDate}"
					style="width: 200px;">
			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-6"></div>
			<div class="col-sm-2">
				<input type="submit" value="更新">
			</div>
		</div>

		<div class="row mt-5">
			<div class="col-sm-1">
				<INPUT type="button" class="btn btn-link" onclick="history.back()"
					value="戻る">
			</div>
		</div>



	</form>
</body>
</html>