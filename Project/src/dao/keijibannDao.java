package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBmanager;
import beans.keijibannBean;

public class keijibannDao {

	public void createContent(String userName, String boardName, String content) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			String sql = "insert into Kboard(user_name, board_name, content, create_date) values (?, ?, ?, NOW())";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userName);
			pStmt.setString(2, boardName);
			pStmt.setString(3, content);



            int result = pStmt.executeUpdate();
            System.out.println(result);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<keijibannBean> findAll(){
		 Connection conn = null;
		 List<keijibannBean> boardList = new ArrayList<keijibannBean>();
	        try {
	        	conn = DBmanager.getConnection();

	        	String sql = "SELECT * FROM Kboard";


	        	Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				while (rs.next()) {
					int id = rs.getInt("id");
					String userName = rs.getString("user_name");
					String title = rs.getString("board_name");
					String content = rs.getString("content");
					Date createDate = rs.getDate("create_date");

					keijibannBean keijibann = new keijibannBean(id, userName, title, content, createDate);

					boardList.add(keijibann);
				}



	        } catch (SQLException e) {
	        	e.printStackTrace();
	} finally {
		 if (conn != null) {
	          	try {
	                		conn.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	            	}
	        	}
	}
			return boardList;

	}

	public keijibannBean getPrimaryId(String id) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "select * from Kboard where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id1 = rs.getInt("id");
			String title = rs.getString("board_name");
			String content = rs.getString("content");
			Date createDate = rs.getDate("create_date");
			String userName = rs.getString("user_name");




			return new keijibannBean(id1, title,  content, createDate, userName);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	public keijibannBean deleteBoard(String kId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "delete from Kboard where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, kId);
			int result = pStmt.executeUpdate();

			System.out.println(result);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public keijibannBean getPrimaryName(String name) {

		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "select * from Kboard where board_name = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id1 = rs.getInt("id");
			String title = rs.getString("board_name");
			String content = rs.getString("content");
			Date createDate = rs.getDate("create_date");
			String userName = rs.getString("user_name");




			return new keijibannBean(id1, title,  content, createDate, userName);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}


}
