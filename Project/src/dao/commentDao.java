package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBmanager;
import beans.commentBean;

public class commentDao {

	public void comment(String content, String username, String boardname) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			String sql = "insert into comment(content, user_name, board_name) values (?, ?, ?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, content);
			pStmt.setString(2, username);
			pStmt.setString(3, boardname);





            int result = pStmt.executeUpdate();
            System.out.println(result);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<commentBean> findAll() {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;
		List<commentBean> commentList = new ArrayList<commentBean>();
        try {
        	conn = DBmanager.getConnection();

        	String sql = "SELECT * FROM comment";


        	Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String title = rs.getString("board_name");
				String content = rs.getString("content");
				String userName = rs.getString("user_name");


				commentBean commentBean = new commentBean(id, content, title, userName);

				commentList.add(commentBean);
			}



        } catch (SQLException e) {
        	e.printStackTrace();
} finally {
	 if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
            	}
        	}
}
		return commentList;


	}

	public List<commentBean> getPrimaryName(String name) {
		Connection conn = null;
		List<commentBean> commentList = new ArrayList<commentBean>();
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "select * from comment INNER JOIN Kboard on comment.board_name = Kboard.board_name where Kboard.board_name = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
			int id = rs.getInt("id");
			String title = rs.getString("board_name");
			String content = rs.getString("content");
			String userName = rs.getString("user_name");
			commentBean comment = new commentBean(id, title,  content, userName);
			commentList.add(comment);}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return commentList;
	}

	public commentBean getPrimaryId(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "select * from comment where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int id1 = rs.getInt("id");
			String title = rs.getString("board_name");
			String content = rs.getString("content");
			String userName = rs.getString("user_name");
			return new commentBean(id1, title,  content, userName);


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public commentBean deleteComment(String comedelete) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "delete from comment where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, comedelete);
			int result = pStmt.executeUpdate();

			System.out.println(result);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}


}
