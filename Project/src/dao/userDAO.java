package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBmanager;
import beans.userBean;



public class userDAO {

	public userBean findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			String result = ango(password);

			// SELECT文を準備
			String sql = "SELECT * FROM user_board WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, result);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("user_name");
			return new userBean(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<userBean> findAll(){
		 Connection conn = null;
		 List<userBean> userList = new ArrayList<userBean>();
	        try {
	        	conn = DBmanager.getConnection();
	        	// 管理者以外を取得するSQL
	        	String sql = "SELECT * FROM user_board where id != 1";

	        	Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				while (rs.next()) {
					int id = rs.getInt("id");
					String loginId = rs.getString("login_id");
					String name = rs.getString("user_name");
					Date birthDate = rs.getDate("birth_date");
					String password = rs.getString("password");
					String createDate = rs.getString("create_date");
					String updateDate = rs.getString("update_date");
					userBean user = new userBean(id, loginId, name, birthDate, password, createDate, updateDate);

					userList.add(user);
				}



	        } catch (SQLException e) {
	        	e.printStackTrace();
	} finally {
		 if (conn != null) {
	          	try {
	                		conn.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	            	}
	        	}
	}
			return userList;

	}

	public void userRegister(String loginId, String userName, String birthDate, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			String result = ango(password);

			// TODO ここに処理を書いていく
			String sql = "insert into user_board(login_id, user_name, birth_date, password, create_date, update_date) values (?, ?, ?, ?, NOW(), NOW())";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, userName);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, result);

			int result1 = pStmt.executeUpdate();

			System.out.println(result1);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String ango(String pass) {
		String source = pass;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {

			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		System.out.println(result);

		return result;

	}

	public static List<userBean> searchUser(String loginId, String userName, String birthStart, String birthEnd) {

		Connection conn = null;
		List<userBean> userList = new ArrayList<userBean>();
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "select * from user_board where id != 1";

			if(!loginId.equals("")) {
			 sql += " AND login_id = '" + loginId + "'";
			}

			if(!userName.equals("")) {
				sql += " and user_name like'%" + userName + "%'";
			}

			if(!birthStart.equals("")) {
				sql += " and birth_date >= '" + birthStart + "'";
			}

			if(!birthEnd.equals("")) {
				sql += " and birth_date <= '" + birthEnd +"'";
			}



			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);


			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId1 = rs.getString("login_id");
				String name = rs.getString("user_name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				userBean user = new userBean(id, loginId1, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;

	}

	public userBean userIdCheck(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();


			String sql = "select * from user_board where login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdData = rs.getString("login_id");
			return new userBean(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	public userBean getPrimaryId(String id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "select * from user_board where id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String loginId = rs.getString("login_id");
			String userName = rs.getString("user_name");
			Date birthDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			return new userBean(loginId, userName, birthDate, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;

	}

	public void updateUser(String password, String userName, String birthDate, String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			String result = ango(password);

			// TODO ここに処理を書いていく
			String sql = "update user_board set password = ?, user_name = ?, birth_date = ? where login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, result);
			pStmt.setString(2, userName);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, loginId);
			int result1 = pStmt.executeUpdate();

			System.out.println(result1);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public userBean deleteUser(String loginId) {
		// TODO 自動生成されたメソッド・スタブ
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// TODO ここに処理を書いていく
			String sql = "delete from user_board where login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			int result = pStmt.executeUpdate();

			System.out.println(result);
			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}