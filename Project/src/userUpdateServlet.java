

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.userBean;
import dao.userDAO;


/**
 * Servlet implementation class userUpdateServlet
 */
@WebServlet("/userUpdateServlet")
public class userUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public userUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id = request.getParameter("id");
		System.out.println(id);
		userDAO userDao = new userDAO();
		userBean user = userDao.getPrimaryId(id);

		request.setAttribute("user", user);

		RequestDispatcher dispatcher =
				request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
				dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");
		String loginId = request.getParameter("loginId");

		if(userName.equals("") || birthDate.equals("")) {
			 request.setAttribute("errMsg", "入力された情報は正しくありません。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userInfUpdate.jsp");
				dispatcher.forward(request, response);
				return;
		}
		else if(!(password.equals(checkPassword))){
			 request.setAttribute("errMsg", "入力された情報は正しくありません。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userInfUpdate.jsp");
				dispatcher.forward(request, response);
				return;
		}
		userDAO userDao = new userDAO();
		userDao.updateUser(password, userName, birthDate, loginId);

		response.sendRedirect("userListServlet");

	}

}
