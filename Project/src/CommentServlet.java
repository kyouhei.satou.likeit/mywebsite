

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.commentBean;
import beans.keijibannBean;
import dao.commentDao;
import dao.keijibannDao;

/**
 * Servlet implementation class CommentServlet
 */
@WebServlet("/CommentServlet")
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CommentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String name = request.getParameter("name");
		System.out.println(name);

		keijibannDao keijibannDao = new keijibannDao();
		keijibannBean board = keijibannDao.getPrimaryName(name);
		request.setAttribute("board", board);

		commentDao commentDao = new commentDao();
		List<commentBean> commenｔ = commentDao.findAll();
		List<commentBean> comment = commentDao.getPrimaryName(name);
		request.setAttribute("comment", comment);



		RequestDispatcher dispatcher =
				request.getRequestDispatcher("/WEB-INF/jsp/comment.jsp");
				dispatcher.forward(request, response);




	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String content = request.getParameter("freeans");
		String userName = request.getParameter("userName");
		String boardName = request.getParameter("boardName");



		 commentDao  commentDao = new commentDao();
		 commentDao.comment(content, userName, boardName);

		 response.sendRedirect("KeijibannEturannServlet");
	}

}
