

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.commentBean;
import beans.keijibannBean;
import dao.commentDao;
import dao.keijibannDao;

/**
 * Servlet implementation class KeijibannEturannServlet
 */
@WebServlet("/KeijibannEturannServlet")
public class KeijibannEturannServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public KeijibannEturannServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		keijibannDao keijibannDao = new keijibannDao();
		commentDao comeDao = new commentDao();
		List<keijibannBean> boardList = keijibannDao.findAll();
		request.setAttribute("boardList", boardList);
		List<commentBean> commentList = comeDao.findAll();
		request.setAttribute("commentList", commentList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/keijibannEturann.jsp");
		dispatcher.forward(request, response);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
