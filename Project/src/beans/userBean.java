package beans;

import java.io.Serializable;
import java.sql.Date;

public class userBean  implements Serializable {

	private int id;

	private String userName;
	private String loginId;
	private String passWord;
	private Date birthDate;
	private String createDate;
	private String updateDate;

	public userBean(String loginId, String name) {
		this.loginId = loginId;
		this.userName = name;
	}



	public userBean
	(String loginId, String name, String password, Date birthDate){
		this.loginId = loginId;
		this.userName = name;
		this.passWord = password;
		this.birthDate = birthDate;

	}

	public userBean(String loginIdData, String nameData, Date birthData, String createData, String updateData) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.loginId = loginIdData;
		this.userName = nameData;
		this.birthDate = birthData;
		this.createDate = createData;
		this.updateDate = updateData;
	}

	public userBean(String loginIdData) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.loginId = loginIdData;
	}


	public userBean(
			int id,
			String loginId,
			String name,
			Date birthDate,
			String password,
			String createDate,
			String updateDate) {
		this.id = id;
		this.loginId= loginId;
		this.userName= name;
		this.birthDate= birthDate;
		this.passWord= password;
		this.createDate= createDate;
		this.updateDate= updateDate;


	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
















}
