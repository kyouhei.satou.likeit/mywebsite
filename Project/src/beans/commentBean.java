package beans;

public class commentBean {

	private int id;
	private String content;
	private String username;
	private String boardname;
	public commentBean(int id2, String content2, String title, String userName2) {
		this.id = id2;
		this.content= content2;
		this.boardname= title;
		this.username= userName2;

		// TODO 自動生成されたコンストラクター・スタブ
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getBoardname() {
		return boardname;
	}
	public void setBoardname(String boardname) {
		this.boardname = boardname;
	}


}
